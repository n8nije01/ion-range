import { Component, OnInit } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  celsius: number;
  fahrenheit: number;

  constructor() {}

  ngOnInit() {
    this.celsius = 0;
    this.fahrenheit = this.celsius * 9 / 5 + 32;
  }

  celsius_changed() {
    this.fahrenheit = this.celsius * 9 / 5 + 32;
  }

  fahrenheit_changed() {
    this.celsius = (this.fahrenheit - 32) * 5 / 9;
  }

}
